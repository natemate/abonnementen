import { exec } from 'child_process'
import fs from 'fs'

const getDirectories = (source) =>
  fs
    .readdirSync(source, { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .map((dirent) => dirent.name)

getDirectories('./domains').forEach((name) =>
  exec(
    `npx pollen -c ./domains/${name}/pollen.config.js -o ./assets/css/themes/${name}/theme.css`
  )
)
