import { mount } from '@vue/test-utils'
import Product from './Product.vue'
import BaseButton from './../../base/BaseButton.vue'
import { products } from '@/mockData'

describe('Vue component Product', () => {
  it('Should render the component correctly', () => {
    // Act
    const propsData = {
      product: products[0],
    }
    const wrapper = mount(Product, {
      propsData,
      components: {
        BaseButton,
      },
      slots: {
        default: `
        <BaseButton
          :secondary="true"
          @click.native="onButtonClick"
          >Bestel nu</BaseButton
        >`,
      },
    })

    // Assert
    expect(wrapper.exists()).toBeTruthy()
    expect(wrapper.element).toMatchSnapshot()
  })

  it('Should show the Weekend title, price and number of included features correctly', () => {
    // Act
    const propsData = {
      product: products[1],
    }
    const wrapper = mount(Product, {
      propsData,
      components: { BaseButton },
      slots: {
        default: `
        <BaseButton
          :secondary="true"
          @click.native="onButtonClick"
          >Bestel nu</BaseButton
        >`,
      },
    })
    const title = wrapper.find('.title')
    const price = wrapper.find('.product-price')
    const features = wrapper.findAll('.product-feature')

    // Assert
    expect(title.text()).toEqual('Weekend')
    expect(price.html()).toContain('4')
    expect(price.html()).toContain(',00')
    expect(features.length).toEqual(4)
  })
})
