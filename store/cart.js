/**
 *
 * @description State: We only refer to the state through getters.
 */
export const state = () => ({
  product: null,
  period: null,
  productSubscription: null,
})

export const mutations = {
  /**
   *
   * @param {Product} payload
   */
  setProduct(state, payload) {
    state.product = payload
  },
  resetProduct(state) {
    state.product = null
  },
  setPeriod(state, payload) {
    state.period = payload
  },
  resetPeriod(state) {
    state.period = null
  },
  setProductSubscription(state, payload) {
    state.productSubscription = payload
  },
  resetProductSubscription(state) {
    state.productSubscription = null
  },
}

export const getters = {
  hasProduct: (state) => Boolean(state.product),
  hasPeriod: (state) => Boolean(state.period),
  hasProductSubscription: (state) => Boolean(state.productSubscription),
  getProduct: (state) => state.product,
  getPeriod: (state) => state.period,
  getProductSubscription: (state) => state.productSubscription,
}

export const actions = {
  /**
   *
   * @param {Product} product the entire product object
   */
  addToCart({ commit }, product) {
    return new Promise((resolve) => {
      commit('setProduct', product)
      resolve()
    })
  },
  /**
   *
   * @param {Period} period the entire period object
   */
  setPeriod({ commit }, period) {
    commit('setPeriod', period)
  },

  /**
   *
   * @param {ProductSubscription} productSubscription the productSubscription object
   */
  setProductSubscription({ commit }, productSubscription) {
    commit('setProductSubscription', productSubscription)
  },

  reset({ commit }) {
    commit('resetProduct')
    commit('resetPeriod')
    commit('resetProductSubscription')
  },
}
