export const products = [
  {
    name: 'Compleet',
    id: 'ADDPC',
    bundle: ['Ma t/m za de papieren krant', 'Ma t/m za de digitale krant'],
    category: 'DIGITAAL + PAPIER',
    highlight: null,
    price: {
      initialPrice: 938,
      discountPrice: 600,
      discountPercentage: 36,
      isIntroductionPrice: true,
      introductionText:
        'De introductietarieven op deze site zijn alleen geldig voor nieuwe abonnees die de afgelopen 6 maanden geen abonnement hebben gehad op het AD',
    },
    features: [
      {
        title: 'Onbeperkt Premiumartikelen',
        description:
          'Elk abonnement op het AD geeft onbeperkt toegang tot alle artikelen in de app en op de nieuwssite. Veel artikelen zijn Premium, te herkennen aan een geel label. Deze artikelen zijn exclusief voor abonnees. Dat zijn de extra waardevolle verhalen die het nieuws duiden, spraakmakende interviews, onze columns en meer.',
      },
      {
        title: 'Digitale krant met bijlagen (pdf)',
        description:
          'De digitale krant is een exacte kopie van de papieren krant. Natuurlijk horen hier ook alle bijlagen en het weekendmagazine Mezza bij. U kunt artikelen delen en bewaren en eenmaal gedownload is de krant ook offline te lezen. Op AD.nl vindt u rechts bovenaan de digitale krant. Voor tablet en smartphone gebruikt u de app. De digitale krant is op werkdagen vanaf 06.00 uur beschikbaar en op zaterdag vanaf 7.00 uur.',
      },
      {
        title: 'Mezza',
        description:
          'Weekendmagazine met een boeiende mix van persoonlijke verhalen, muziek, film, boeken, mode en interieur. Geniet het hele weekend van uitgebreide reportages en diepgaande interviews.',
      },
      {
        title: 'Extra dikke weekendkrant',
        description:
          'De zaterdagkrant van het AD wordt op zaterdag voor 09.00 uur thuisbezorgd en biedt urenlang lees- en bladerplezier met alle verhalen, rubrieken, columns, puzzels en fotoreportages.',
      },
      {
        title: 'Doordeweeks de papieren krant',
        description:
          "Het AD bestaat uit drie katernen: Nationaal en internationaal nieuws: Nieuws uit binnen-en buitenland, analyses en achtergronden. Sportwereld: Meer dan 16 pagina's leesplezier over sport. Landelijk en internationaal sportnieuws, maar ook uit uw eigen regio. Regionaal nieuws: Het nieuws uit uw eigen regio en buurt. U krijgt automatisch de editie toegestuurd die hoort bij uw woonplaats. Als u niet in één van deze regio's woont, krijgt u de landelijke editie van het AD.",
      },
    ],
  },
  {
    name: 'Weekend',
    id: 'ADDPW',
    bundle: ['Zaterdag de papieren krant', 'Ma t/m za de digitale krant'],
    category: 'DIGITAAL + PAPIER',
    highlight: 'MEEST GEKOZEN',
    price: {
      initialPrice: 624,
      discountPrice: 400,
      discountPercentage: 36,
      isIntroductionPrice: true,
      introductionText:
        'De introductietarieven op deze site zijn alleen geldig voor nieuwe abonnees die de afgelopen 6 maanden geen abonnement hebben gehad op het AD',
    },
    features: [
      {
        title: 'Onbeperkt Premiumartikelen',
        description:
          'Elk abonnement op het AD geeft onbeperkt toegang tot alle artikelen in de app en op de nieuwssite. Veel artikelen zijn Premium, te herkennen aan een geel label. Deze artikelen zijn exclusief voor abonnees. Dat zijn de extra waardevolle verhalen die het nieuws duiden, spraakmakende interviews, onze columns en meer.',
      },
      {
        title: 'Digitale krant met bijlagen (pdf)',
        description:
          'De digitale krant is een exacte kopie van de papieren krant. Natuurlijk horen hier ook alle bijlagen en het weekendmagazine Mezza bij. U kunt artikelen delen en bewaren en eenmaal gedownload is de krant ook offline te lezen. Op AD.nl vindt u rechts bovenaan de digitale krant. Voor tablet en smartphone gebruikt u de app. De digitale krant is op werkdagen vanaf 06.00 uur beschikbaar en op zaterdag vanaf 7.00 uur.',
      },
      {
        title: 'Mezza',
        description:
          'Weekendmagazine met een boeiende mix van persoonlijke verhalen, muziek, film, boeken, mode en interieur. Geniet het hele weekend van uitgebreide reportages en diepgaande interviews.',
      },
      {
        title: 'Extra dikke weekendkrant',
        description:
          'De zaterdagkrant van het AD wordt op zaterdag voor 09.00 uur thuisbezorgd en biedt urenlang lees- en bladerplezier met alle verhalen, rubrieken, columns, puzzels en fotoreportages.',
      },
    ],
  },
  {
    name: 'Digitaal',
    id: 'ADD',
    bundle: ['Ma t/m za de digitale krant', 'Onbeperkt Premiumartikelen'],
    category: 'DIGITAAL',
    highlight: null,
    price: {
      initialPrice: 549,
      discountPrice: 350,
      discountPercentage: 36,
      isIntroductionPrice: true,
      introductionText:
        'De introductietarieven op deze site zijn alleen geldig voor nieuwe abonnees die de afgelopen 6 maanden geen abonnement hebben gehad op het AD',
    },
    features: [
      {
        title: 'Onbeperkt Premiumartikelen',
        description:
          'Elk abonnement op het AD geeft onbeperkt toegang tot alle artikelen in de app en op de nieuwssite. Veel artikelen zijn Premium, te herkennen aan een geel label. Deze artikelen zijn exclusief voor abonnees. Dat zijn de extra waardevolle verhalen die het nieuws duiden, spraakmakende interviews, onze columns en meer.',
      },
      {
        title: 'Digitale krant met bijlagen (pdf)',
        description:
          'De digitale krant is een exacte kopie van de papieren krant. Natuurlijk horen hier ook alle bijlagen en het weekendmagazine Mezza bij. U kunt artikelen delen en bewaren en eenmaal gedownload is de krant ook offline te lezen. Op AD.nl vindt u rechts bovenaan de digitale krant. Voor tablet en smartphone gebruikt u de app. De digitale krant is op werkdagen vanaf 06.00 uur beschikbaar en op zaterdag vanaf 7.00 uur.',
      },
    ],
  },
  {
    name: 'Digitaal Basis',
    id: 'ADDDB',
    bundle: ['Onbeperkt Premiumartikelen'],
    category: 'DIGITAAL',
    highlight: null,
    price: {
      initialPrice: 199,
      discountPrice: 120,
      discountPercentage: 40,
      isIntroductionPrice: true,
      introductionText:
        'De introductietarieven op deze site zijn alleen geldig voor nieuwe abonnees die de afgelopen 6 maanden geen abonnement hebben gehad op het AD',
    },
    features: [
      {
        title: 'Onbeperkt Premiumartikelen',
        description:
          'Elk abonnement op het AD geeft onbeperkt toegang tot alle artikelen in de app en op de nieuwssite. Veel artikelen zijn Premium, te herkennen aan een geel label. Deze artikelen zijn exclusief voor abonnees. Dat zijn de extra waardevolle verhalen die het nieuws duiden, spraakmakende interviews, onze columns en meer.',
      },
    ],
  },
]

export const periods = [
  {
    name: '1 jaar',
    id: '1YR',
    years: 1,
    months: 12,
    days: 365,
    discountPercentage: 0,
  },
  {
    name: '2 jaar',
    id: '2YR',
    years: 2,
    months: 24,
    days: 730,
    discountPercentage: 5,
  },
  {
    name: '3 jaar',
    id: '3YR',
    years: 1,
    months: 36,
    days: 1095,
    discountPercentage: 10,
  },
]
