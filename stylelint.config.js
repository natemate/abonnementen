module.exports = {
  extends: [
    'stylelint-config-standard',
    'stylelint-config-prettier',
    'stylelint-config-recommended-vue',
  ],
  // add your custom config here
  // https://stylelint.io/user-guide/configuration
  ignoreFiles: [
    'assets/css/themes/**/theme.css',
    'assets/css/fonts.css',
    'assets/css/tailwind.css',
  ],
  rules: {
    'at-rule-no-unknown': null,
  },
}
