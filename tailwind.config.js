/**
 * any color referenced by tailwind utilities (text-, bg-)
 * should point to the var() from theme.css
 */
const theme = require('./domains/ad.nl/pollen.config')

const colors = theme.modules.color
const zIndex = theme.modules.layer

module.exports = {
  mode: 'jit',
  content: [],
  theme: {
    container: {
      center: true,
      padding: `var(--size-2)`,
    },
    colors: { ...colors },
    zIndex: { ...zIndex },
  },
  plugins: [],
}
