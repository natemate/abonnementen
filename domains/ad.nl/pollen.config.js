/* eslint-disable */
const { defineConfig } = require('pollen-css/utils')

const theme = {
  color: {
    primary: `var(--color-red-700)`,
    secondary: `var(--color-grey-800)`,
    'highlight-1': `var(--color-grey-200)`,
    'highlight-2': `var(--color-grey-400)`,
    'highlight-3': `var(--color-yellow-500)`,
    success: `var(--color-green)`,
    error: `var(--color-red)`,
    'text-1': `var(--color-grey-900)`,
    'text-2': `var(--color-grey-500)`,
    white: 'white',
  },
  font: {
    sans: 'Open Sans, system-ui, -apple-system, Segoe UI, Roboto, Noto Sans',
  },
}

module.exports = defineConfig((defaults) => ({
  modules: {
    font: {
      ...defaults.font,
      ...theme.font,
    },
    color: {
      ...defaults.color,
      ...theme.color,
    },
    layer: {
      below: -1,
      top: 2147483647,
      10: 10,
      20: 20,
      30: 30,
      40: 40,
      50: 50,
    },
  },
}))
