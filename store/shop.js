/**
 *
 * @description State: We only refer to the state through getters.
 */
export const state = () => ({
  products: [],
  periods: [],
})

export const getters = {
  hasProducts: (state) => Boolean(state.products.length),
  getProducts: (state) => state.products,
  hasPeriods: (state) => Boolean(state.periods.length),
  getPeriods: (state) => state.periods,
}

export const mutations = {
  setProducts(state, payload) {
    state.products = payload
  },
  setPeriods(state, payload) {
    state.periods = payload
  },
}

export const actions = {
  async prepare({ commit }) {
    const { products } = await this.$http.$get('/products.json')
    const { periods } = await this.$http.$get('/periods.json')
    commit('setProducts', products)
    commit('setPeriods', periods)
  },
}
